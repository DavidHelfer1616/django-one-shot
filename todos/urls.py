from django.urls import path
from todos.views import create_list, create_task, delete_list, todo_lists, todo_tasks, update_list, update_task


urlpatterns = [
    path('',todo_lists, name="todo_list_list"),
    path('<int:id>', todo_tasks, name="todo_list_detail"),
    path('create/', create_list, name="todo_list_create"),
    path('<int:id>/edit/', update_list, name="todo_list_update"),
    path('<int:id>/delete/', delete_list, name="todo_list_delete"),
    path('items/create/', create_task, name="todo_item_create"),
    path('items/<int:id>/edit/', update_task, name="todo_item_update")
]
