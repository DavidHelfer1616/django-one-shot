from django.contrib import admin
from todos.models import TodoItem, TodoList

@admin.register(TodoList)
class TodoAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
    ]

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]